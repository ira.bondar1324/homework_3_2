const headerButton=document.querySelector(".header-main__button");
const headerMenu = document.querySelector(".navbar-main__list");
headerButton.addEventListener("click",()=>{
    headerButton.classList.toggle("header-button--active");
    headerMenu.classList.toggle("navbar-list--active");
    if (headerButton.classList.contains("header-button--active")){
        headerButton.innerHTML=`<svg width="15" height="14" viewBox="0 0 15 14" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="12.8033" y="13.7175" width="17" height="2" transform="rotate(-135 12.8033 13.7175)" fill="white"/>
        <rect x="0.782471" y="12.3033" width="17" height="2" transform="rotate(-45 0.782471 12.3033)" fill="white"/>
        </svg>`;
    } else{
        headerButton.innerHTML=`<svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect width="17" height="2" fill="white"/>
        <rect y="4" width="17" height="2" fill="white"/>
        <rect y="8" width="17" height="2" fill="white"/></svg>`;
    }
});