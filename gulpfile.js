import gulp from "gulp";
const {parallel, series, src, dest, watch}=gulp;
import clean from "gulp-clean";
import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);
import autoPrefixer from "gulp-autoprefixer";
import concat from "gulp-concat";
import minifyjs from "gulp-js-minify";
import cleanCSS from "gulp-clean-css";
import imagemin from "gulp-imagemin";
import browserSinc from "browser-sync";
const bsServer = browserSinc.create();

export function cleaning(){
    return gulp.src("./dist/",{read:false})
    .pipe(clean());
}

export function styles(){
    return gulp.src("./src/styles/style.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(autoPrefixer({
        cascade: false
    }))
    .pipe(cleanCSS())
    .pipe(gulp.dest("./dist/css/"))
    .pipe(bsServer.reload({stream:true}));
}

export function scripts(){
     return gulp.src("./src/js/*.js").pipe(concat("scripts.min.js"))
    .pipe(minifyjs())
    .pipe(gulp.dest("./dist/js/"))
    .pipe(bsServer.reload({stream:true}));
}

export function images(){
    return gulp.src('src/img/*')
	.pipe(imagemin())
	.pipe(gulp.dest('dist/img'))
    .pipe(bsServer.reload({stream:true}));
}


gulp.task("dev",()=>{
    bsServer.init({
        server:{
            baseDir:"./",
            browser:"google chrome",
        },
    })

    watch("./src/img/**/*.{jpg,jpeg,png,svg,webp}").on("change",images);
    watch("./src/styles/**/*.scss").on("change",styles);
    watch("./src/js/index.js").on("change",scripts);
    watch("./index.html").on("change",bsServer.reload);
})

gulp.task("build",function (done){
    cleaning,images(),styles(),scripts();
    done();
});